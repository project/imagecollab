<?php

namespace Drupal\imagecollab\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ImageCollabSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'imagecollab.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imagecollab_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['uploads_per_point'] = [
      '#type' => 'number',
      '#title' => $this->t('Uploads per point'),
      '#default_value' => $config->get('uploads_per_point'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('uploads_per_point', $form_state->getValue('uploads_per_point'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
