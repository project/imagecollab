<?php

namespace Drupal\imagecollab\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for imagecollab image entity.
 */
class ImageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\imagecollab\Entity\Image $image */
    $image = $this->entity;

    $isAdmin = $this->currentUser()->hasPermission('administer imagecollab');

    // Hide fields unless user is an admin.
    // @todo consider using field access for this.
    $form['uid']['#access'] = $isAdmin;
    $form['chain']['#access'] = $isAdmin;
    $form['status']['#access'] = $isAdmin;
    $form['place']['#access'] = $isAdmin;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the image.'));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the image.'));
    }
    $form_state->setRedirect('entity.imagecollab_image.canonical', ['imagecollab_image' => $entity->id()]);
  }

}
