<?php

namespace Drupal\imagecollab\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting imagecollab image entities.
 */
class ImageDeleteForm extends ContentEntityDeleteForm {

  // @todo error checking to prevent chain orphans.
}
