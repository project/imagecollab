<?php

namespace Drupal\imagecollab\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for imagecollab chain edit forms.
 */
class ChainForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\imagecollab\Entity\Chain $chain */
    $chain = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Default entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Default entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.imagecollab_chain.canonical', ['imagecollab_chain' => $entity->id()]);
  }

}
