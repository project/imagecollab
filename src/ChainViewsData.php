<?php

namespace Drupal\imagecollab;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the imagecollab chain entity type.
 */
class ChainViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }

}
