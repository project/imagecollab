<?php

namespace Drupal\imagecollab\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\file\Entity\File;
use Drupal\imagecollab\ImageInterface;
use Drupal\user\UserInterface;

/**
 * Defines the imagecollab image entity.
 *
 * @ContentEntityType(
 *   id = "imagecollab_image",
 *   label = @Translation("Image"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "form" = {
 *       "default" = "Drupal\imagecollab\Form\ImageForm",
 *       "delete" = "Drupal\imagecollab\Form\ImageDeleteForm",
 *     },
 *     "access" = "Drupal\imagecollab\ImageCollabAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "imagecollab_image",
 *   admin_permission = "administer imagecollab",
 *   entity_keys = {
 *     "id" = "image_id",
 *     "label" = "image_id",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/imagecollab/image/{imagecollab_image}",
 *     "add-form" = "/imagecollab/image/add",
 *     "edit-form" = "/imagecollab/image/{imagecollab_image}/edit",
 *     "delete-form" = "/imagecollab/image/{imagecollab_image}/delete",
 *     "collection" = "/imagecollab/image",
 *   }
 * )
 */
class Image extends ContentEntityBase implements ImageInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (empty($this->get('chain')->target_id)) {
      $chain = Chain::create();
      $chain->save();
      $this->set('chain', $chain->id());
      $this->set('place', 1);
    }
    else {
      $chain_id = $this->get('chain')->target_id;
      $chain = Chain::load($chain_id);
      if (empty($this->get('place')->value)) {
        $this->set('place', $chain->getImageCount() + 1);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    $chain_id = $this->get('chain')->target_id;

    // Rename image file.
    $file = File::load($this->get('fid')->target_id);
    $ext = array_pop(explode('.', $file->getFileUri()));
    $new = 'public://imagecollab/' . $chain_id . '_' . $this->id() . '.' . $ext;
    \Drupal::service('file.repository')->move($file, $new);

    // Update chain.
    $chain = Chain::load($chain_id);
    $chain->set('artist_last', \Drupal::currentUser()->id());
    $chain->set('artist_count', $chain->getArtistCount());
    $chain->set('image_count', $chain->getImageCount());
    $chain->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['fid'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setDescription(t('The image file.'))
      ->setRequired(TRUE)
      ->setSettings([
        'file_directory' => 'imagecollab',
        'alt_field' => FALSE,
        'alt_field_required' => FALSE,
        'file_extensions' => 'png jpg jpeg',
        // @todo to make this configurable we might not want to define it here.
        'min_resolution' => '800x600',
        'max_resolution' => '800x600',
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'default',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'label' => 'hidden',
        'type' => 'image_image',
        'weight' => 0,
      ]);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Artist'))
      ->setDescription(t('The artist who created the image.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '20',
        ],
      ]);

    $fields['chain'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Chain'))
      ->setDescription(t('The image chain.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'imagecollab_chain')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '20',
        ],
      ]);

    $fields['place'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Place'))
      ->setDescription(t('The place of this image in the chain.'))
      ->setSetting('min', 1)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => 0,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 0,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time the image entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time the image entity was last edited.'));

    return $fields;
  }

}
