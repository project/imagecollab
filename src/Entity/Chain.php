<?php

namespace Drupal\imagecollab\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\imagecollab\ChainInterface;
use Drupal\user\UserInterface;

/**
 * Defines the imagecollab chain entity.
 *
 * @ContentEntityType(
 *   id = "imagecollab_chain",
 *   label = @Translation("Chain"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "form" = {
 *       "default" = "Drupal\imagecollab\Form\ChainForm",
 *     },
 *     "access" = "Drupal\imagecollab\ImageCollabAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\imagecollab\ChainViewsData",
 *   },
 *   base_table = "imagecollab_chain",
 *   admin_permission = "administer imagecollab",
 *   entity_keys = {
 *     "id" = "chain_id",
 *     "label" = "chain_id",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "uid" = "uid_first",
 *   },
 *   links = {
 *     "canonical" = "/imagecollab/chain/{imagecollab_chain}",
 *     "edit-form" = "/imagecollab/chain/{imagecollab_chain}/edit",
 *     "collection" = "/imagecollab/chain",
 *   }
 * )
 */
class Chain extends ContentEntityBase implements ChainInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $uid = \Drupal::currentUser()->id();
    $values += [
      'artist_first' => $uid,
      'artist_last' => $uid,
      'artist_count' => 1,
      'image_count' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('artist_first')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('artist_first')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('artist_first', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('artist_first', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['artist_first'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('First artist'))
      ->setDescription(t('The artist who started the chain.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '20',
        ],
      ]);

    $fields['artist_last'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Last artist'))
      ->setDescription(t('The artist who last added an image to the chain.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '20',
        ],
      ]);

    $fields['artist_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of Artists'))
      ->setDescription(t('Total number of artists participating in the chain.'))
      ->setRequired(TRUE)
      ->setSetting('min', 1)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => 0,
      ]);

    $fields['image_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of Images'))
      ->setDescription(t('Total number of images in the chain.'))
      ->setRequired(TRUE)
      ->setSetting('min', 1)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => 0,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 0,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time the entity was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ]);

    return $fields;
  }

  /**
   * Entity query service.
   */
  protected function entityQuery() {
    return \Drupal::entityQuery('imagecollab_image');
  }

  /**
   * Try to deprecate this.
   */
  protected function database() {
    return \Drupal::database();
  }

  /**
   * {@inheritdoc}
   */
  public function getArtistCount() {
    // @todo could this be entity query?
    $database = $this->database();
    $query = $database->select('imagecollab_image', 'i')
      ->condition('i.chain', $this->id())
      ->fields('i', ['uid'])
      ->distinct();
    $result = $query->countQuery()->execute()->fetchField();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageCount() {
    // @todo could this be entity query?
    $database = $this->database();
    $query = $database->select('imagecollab_image', 'i')
      ->condition('i.chain', $this->id())
      ->fields('i', ['image_id']);
    $result = $query->countQuery()->execute()->fetchField();
    return $result;
  }

}
