<?php

namespace Drupal\imagecollab\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\imagecollab\ChainInterface;

/**
 * Returns responses for imagecollab image routes.
 */
class ImageController extends ControllerBase {

  /**
   * Form to add an image to an existing chain.
   *
   * @param \Drupal\imagecollab\ChainInterface $chain
   *   The imagecollab chain entity.
   *
   * @return array
   *   The form structure or render array.
   */
  public function addImage(ChainInterface $chain) {
    $image = $this->entityTypeManager()->getStorage('imagecollab_image')->create([
      'chain' => $chain->id(),
    ]);
    $form = $this->entityFormBuilder()->getForm($image);
    return $form;
  }

  /**
   * Checks access to add an image to a chain.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\imagecollab\ChainInterface $chain
   *   The imagecollab chain entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, ChainInterface $chain) {
    $notLastArtist = $chain->get('artist_last')->target_id != $account->id();
    return AccessResult::allowedIf($account->hasPermission('submit imagecollab images') and $notLastArtist);
  }

}
