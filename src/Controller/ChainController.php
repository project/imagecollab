<?php

namespace Drupal\imagecollab\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for imagecollab chain routes.
 */
class ChainController extends ControllerBase {

  /**
   * Form to start a new chain by uploading the first image.
   *
   * @return array
   *   The form structure or render array.
   */
  public function startChain() {
    $image = $this->entityTypeManager()->getStorage('imagecollab_image')->create();
    $form = $this->entityFormBuilder()->getForm($image);
    return $form;
  }

}
