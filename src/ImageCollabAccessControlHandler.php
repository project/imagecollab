<?php

namespace Drupal\imagecollab;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for imagecollab entities.
 */
class ImageCollabAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished imagecollab images');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published imagecollab images');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'administer imagecollab');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'administer imagecollab');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'submit imagecollab images');
  }

}
