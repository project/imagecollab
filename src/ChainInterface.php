<?php

namespace Drupal\imagecollab;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Abstract class for imagecollab chain entities.
 */
interface ChainInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the chain entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Default entity.
   */
  public function getCreatedTime();

  /**
   * Sets the chain entity creation timestamp.
   *
   * @param int $timestamp
   *   The chain entity creation timestamp.
   *
   * @return \Drupal\imagecollab\Entity\Chain
   *   The called chain entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the total number of images in this chain.
   *
   * @return int
   *   The number of images in the chain.
   */
  public function getImageCount();

  /**
   * Gets the number of artists participating in this chain.
   *
   * @return int
   *   The number of artists in the chain.
   */
  public function getArtistCount();

}
