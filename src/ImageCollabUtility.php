<?php

namespace Drupal\imagecollab;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserData;

/**
 * The imagecollab utility service.
 */
class ImageCollabUtility {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserData
   */
  protected $userData;

  protected $chainStorage;

  protected $imageStorage;

  /**
   * A chain entity to act on.
   *
   * @var \Drupal\imagecollab\ChainInterface
   */
  protected $chain;

  /**
   * An image entity to act on.
   *
   * @var \Drupal\imagecollab\ImageInterface
   */
  protected $image;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\user\UserData $user_data
   *   The user data service.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user,
    UserData $user_data
  ) {
    $this->database = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->userData = $user_data;

    $this->chainStorage = $this->entityTypeManager->getStorage('imagecollab_chain');
    $this->imageStorage = $this->entityTypeManager->getStorage('imagecollab_image');

    $this->chain = NULL;
    $this->image = NULL;
  }

  /**
   *
   */
  public function setChain(ChainInterface $chain) {
    $this->chain = $chain;
    return $this;
  }

  /**
   * Gets the image entities in this chain.
   *
   * @return \Drupal\imagecollab\ImageInterface[]
   *   An array of image entities.
   */
  public function getImages() {
    // @todo check that $this->chain is set.
    $images = $this->imageStorage->loadByProperties(
      ['chain' => $this->chain->id()]
    );
    sort($images);
    return $images;
  }

  /**
   *
   */
  public function setImage(ImageInterface $image) {
    $this->image = $image;
    return $this;
  }

  /**
   * Loads a chain entity based on one of its images.
   *
   * @return \Drupal\imagecollab\ChainInterface
   *   The chain entity the image belongs to.
   */
  public function getChainFromImage() {
    // @todo check that $this->image is set.
    $chain_id = $this->image->get('chain')->target_id;
    $chain = $this->chainStorage->load($chain_id);
    return $chain;
  }

  /**
   *
   */
  public function getImageSiblings() {
    // @todo check that $this->image is set.
    $siblings = [];
    $db = ['back' => ['<', 'DESC'], 'next' => ['>', 'ASC']];
    foreach ($db as $key => $cond) {
      $query = $this->database->select('imagecollab_image', 'i')
        ->condition('i.chain', $this->image->get('chain')->target_id)
        ->condition('i.created', $this->image->get('created')->value, $cond[0])
        ->fields('i', ['image_id'])
        ->orderBy('i.created', $cond[1])
        ->range(0, 1);
      $siblings[$key] = $query->execute()->fetchField();
    }
    return $siblings;
  }

}
