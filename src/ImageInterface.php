<?php

namespace Drupal\imagecollab;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Abstract class for imagecollab image entities.
 */
interface ImageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the image entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the image entity.
   */
  public function getCreatedTime();

  /**
   * Sets the image entity creation timestamp.
   *
   * @param int $timestamp
   *   The image entity creation timestamp.
   *
   * @return \Drupal\imagecollab\Entity\Image
   *   The called image entity.
   */
  public function setCreatedTime($timestamp);

}
